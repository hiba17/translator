# ** Lost in Translation**
Lost in translation is an app  that allows you to register a name, and translate sentences as well as words into signlanguage. Your profile page shows you the 10 last logged inputs you wrote, as well as logs you out and clears the storage so that another person can try.

## Heroku page
I tried, but I didn't get it to work

## Technologies
- visual Studio Code
- Javascript
- React
- HTML
- Node.js
- CSS
- Heroku
-React Router Dom

## Files

### App.js
Links the js components based on the path.

- '/' = LoginPage
- '/translator' = Translator page
- '/profile' = Profile page

### header.js
is on all the pages and shows the app-name as well as who is logged it at the moment

### login.js
Starts the react app with a title animation as well as an input bok to write your name, and a button to move on to the next page. Id you try to log in multiple times without logging out it will give you an alert.


### translator.js
Input section where you can write what you want to translate, English letters only and no symbols allowed.
If you are not logged inn it will give you a message.once the button beside it is clicked it will show you the signs that corresponds to your written sentence. There is also a button (profile) that will lead you to the profile page

### profile.js
Here you will get the 10 last inputs you wrote , as well as a log out button.

