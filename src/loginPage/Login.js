import React, { useState } from "react";
import { useHistory} from "react-router-dom";
import './Login.css'

function Login() {
    const [name, setName] = useState()
    const history = useHistory();

    //function that sets name to be the value user writes in
    const changeName= e => {
        setName(e.target.value);
      };
      const moveToTranslatorPage= e => {
        //saving the name to the session Storage as long as we don't already have a name value in the storage
        //if we do, we give an alert that used should log out first, before registering a new name
        if(sessionStorage.getItem('name') !== null){
          return alert( "Go to profile and log out , you are already logged.")
        }else{
          sessionStorage.setItem('name', name)
          //navigating the history instance to the translator component
          history.push( '/translator' )
        }       
      };


    return (
      //Animation of the title
        <div className="login">
          <h1>
          <span>L</span>
          <span>O</span>
          <span>S</span>
          <span>T</span>
          <span> </span>
          <span>I</span>
          <span>N</span>
          <span> </span>
          <span>T</span>
          <span>R</span>
          <span>A</span>
          <span>N</span>
          <span>S</span>
          <span>L</span>
          <span>A</span>
          <span>T</span>
          <span>I</span>
          <span>O</span>
          <span>N</span>
          </h1>
            <input type="text" className="login__name" placeholder="What is you name?" onChange={changeName}/>
            <button className="login__button" onClick={moveToTranslatorPage} ></button>
        </div>
    )
}

export default Login
