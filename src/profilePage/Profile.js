import React, { useState } from 'react'
import './Profile.css'
import { useHistory} from "react-router-dom";
function Profile() {
  
    //Find the lenght of the storage
    const [keys] = useState(Object.keys(sessionStorage))
    const [index] = useState(keys.length)
    const history = useHistory();  
    
    //clear the entire storage and redirect back to the main login page
    const RemoveAndLoad= e => {
        sessionStorage.clear()
        history.push({  pathname: '/'})
      };
    //loop through each item in the storage and get the value out displayed in a <th> tag
    //add it to a list and return this list
    const displayResults =() =>{
         let arr = []
        for (let i = 0; i < index; i++) {
          if(keys[i] !== "name"){
            arr.push(<tr key={i}>
                <th>{sessionStorage.getItem(keys[i]) }</th>
            </tr>)
          }
        }     
        return arr
    }
    

    return (
        <div className="profile">
          <h2 className ="profile__title" >Profile</h2>
      <table className="table">
        <tbody>
        <tr>
          <th>Logged: </th>
        </tr>
                {displayResults()} 
         </tbody>
      </table>

      <button className="profile__RemoveAndLoad__button" onClick={RemoveAndLoad} > Log out </button>
      
      
        </div>
    )
}

export default Profile
