import logo from './logo.svg';
import './App.css'
import Header from './Header'
import Login from './loginPage/Login'
import Translator from './translatorPage/Translator'
import Profile from './profilePage/Profile'
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";

function App() {
  return (
    //Switches from different componetns given the path-ending
    <Router>
    <div className="App">   
      <Switch>

      <Route path="/profile">
        <Header/>
        <Profile/>
        </Route>

        <Route path="/translator">
        <Header/>
        <Translator/>
        </Route>

        <Route path="/">
        <Header/>
        <Login/>
        </Route>

      </Switch>
     
    </div>
    </Router>
  );
}

export default App;
