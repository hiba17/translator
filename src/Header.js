import React, { useState,useEffect } from "react";

import './Header.css'

function Header() {
     const [name, setName] = useState()
    //This is the name of the registered user. it changes  everytime the storage key"name"
    //changes, if it doesn't exist , we display no name in the header
    useEffect(()=> {
        setName(sessionStorage.getItem("name"))
    },[sessionStorage.getItem("name")])
    return (
        <div className="header">
            <div className="header__title">Lost in Translation</div>
            <div className="header__login">
            <img className="header__login__logo"  width="30px" src="password.png" alt=""/>
            <p className="header__login__name">{name}</p>
            </div>
        </div>
    )
}

export default Header
