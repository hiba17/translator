import React from 'react'
import './SignsComponent'

function FindSignLetter({sentence}) {
   
   
    //at every reload we use the array sent in which is a word/sentence, loop through each letter
    //and use that letter to concatinate a string soruce that is the path for each image
  const displaySigns =() =>{
       let sentenceToSigns = [];
     for(let i = 0; i < sentence.length; i++) {
       let source= "individial_signs/" + sentence[i] + ".png"
        sentenceToSigns.push(
          <img key={i} src={source} alt="" />
        );
      }
      return sentenceToSigns


     }
    return (
        <div>
            <h3>Translation: </h3>
            {displaySigns()}   
        </div>
    )
}

export default FindSignLetter
