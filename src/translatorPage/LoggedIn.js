import React, { useState} from "react";
import SignComponent from "./SignsComponent";
import { useHistory} from "react-router-dom";
import './LoggedIn.css'
function LoggedIn() {
    //setting only one sentence at a time
    const [sentence, setSentence] = useState()
    //This is where we set all the sentences,start off empty
    const [array, setArray] = useState([])
    //maxStorage is 10,given in the spec meaning we can only load 10 sentences at a time
    const [maxStorage] = useState(10)
    //key is used to keep track of how many sentences we have loaded. starts at 0
    const [key, setKey] = useState(0)
    //use this constant to keep track of which item to remove next,starts at 0
    const [removeSentenceOnIndex, setRemoveSentenceOnIndex] = useState(0)
    const history = useHistory();
    
   
       const changeSentence= e => {
          setSentence(e.target.value);
       };
       const setSignComponent = function() {
         //We replace all symbols non englishletters and/or spaces with ""
        let sentenceWithRemovedSymbols=sentence.replace(/[^0-9a-z-A-Z ]/g, "").replaceAll(/\s/g,'')
        //turn the string into an array and set it to our const array
        let array =[...sentenceWithRemovedSymbols]
        setArray(array)
        storeSentence()
    
       };
      const storeSentence = function(){
        //StoreSentence checks if we haven't reach max storage space,
        // if we haven't  we use the key to set in the sentance in storage and increment the key
        //if we have we use the removeSentenceONindex to remove the first item and add the next
        //increment both of the indexes
           if(sessionStorage.length<=maxStorage){
            sessionStorage.setItem(key,sentence)
            setKey(prev => prev + 1);
           }else{
              sessionStorage.removeItem(removeSentenceOnIndex)
              setRemoveSentenceOnIndex(prev => prev + 1);
              sessionStorage.setItem(key,sentence)
              setKey(prev => prev + 1);
           }      
      }
      //redirect to the profilecomponent
      const loadProfile= e => {
        history.push('/profile')
      };
    return (
      //signcomponent is the conponent that finds all the sign equal to the sentence sent it
        <div>
            <input placeholder="What you want to translate " type="text" className="translator__name" onChange={changeSentence} />
            <button className="translator__setSign__button" onClick={setSignComponent}/>
            <SignComponent sentence={array}/>
            <img src={array} alt=""></img>
            <button className="translator__loadProfile__button" onClick={loadProfile} > Profile </button>
           
        </div>
    )
}

export default LoggedIn
