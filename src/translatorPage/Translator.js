import React from "react";
import LoggedIn from "./LoggedIn";

function Translator() {
  

    return (
        // if   we haven't registered a name we tel user you have to log in first
        //if they have, then we display Loggedin component
        <div className="translator">
           
            {sessionStorage.getItem("name") !== null ?(
                <LoggedIn></LoggedIn>):(
               <h3>You have to log in first </h3>
            )

            }
            
           
        </div>
    )
}

export default Translator
